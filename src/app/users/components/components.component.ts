import { Component } from '@angular/core';

@Component({
  selector: 'xspon-components',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ComponentsComponent {
}
