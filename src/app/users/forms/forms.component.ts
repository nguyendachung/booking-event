import { Component } from '@angular/core';

@Component({
  selector: 'xspon-form-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class FormsComponent {
}
