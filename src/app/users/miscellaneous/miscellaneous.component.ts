import { Component } from '@angular/core';

@Component({
  selector: 'xspon-miscellaneous',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MiscellaneousComponent {
}
