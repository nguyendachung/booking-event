import { Component } from '@angular/core';

@Component({
  selector: 'xspon-maps',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MapsComponent {
}
