import { Component } from '@angular/core';

@Component({
  selector: 'xspon-completed-card',
  styleUrls: ['./completed.component.scss'],
  templateUrl: './completed.component.html',
})
export class CompletedComponent {

  flipped = false;

  toggleView() {
    this.flipped = !this.flipped;
  }
}
