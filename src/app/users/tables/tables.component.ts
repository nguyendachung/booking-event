import { Component } from '@angular/core';

@Component({
  selector: 'xspon-tables',
  template: `<router-outlet></router-outlet>`,
})
export class TablesComponent {
}
