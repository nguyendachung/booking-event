import { Component } from '@angular/core';

@Component({
  selector: 'xspon-chartjs',
  styleUrls: ['./chartjs.component.scss'],
  templateUrl: './chartjs.component.html',
})
export class ChartjsComponent {}
