import { Component } from '@angular/core';

@Component({
  selector: 'xspon-charts',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ChartsComponent {
}
