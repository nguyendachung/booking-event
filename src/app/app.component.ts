import { Component } from '@angular/core';

@Component({
  selector: 'xspon-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {}
